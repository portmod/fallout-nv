# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.nexus import NexusMod
from common.fallout_nv import FalloutNV
from pybuild import InstallDir


class Package(NexusMod, FalloutNV):
    NAME = "ETJ Miniguns"
    DESC = "Custom animations, meshes, textures and sounds for the miniguns"
    HOMEPAGE = "https://www.nexusmods.com/newvegas/mods/78325"
    KEYWORDS = "fallout-nv"
    LICENSE = "all-rights-reserved"
    IUSE = "vanilla-stats"
    RDEPEND = """
        dev-util/xnvse
        dev-util/knvse
        dev-util/jip
        dev-util/johnny-guitar
        dev-util/showoff
        assets-animations/iscontrol-enabler
    """
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/newvegas/mods/78325?tab=files&file_id=1000098803
        -> ETJ_Miniguns-78325-0-9-1662747826.rar
    """
    # WAV files don't work in BSAs :|
    BSA_EXCLUDE = ["Sound/FX"]
    INSTALL_DIRS = [
        InstallDir(".", BLACKLIST=["meta.ini"], REQUIRED_USE="!vanilla-stats"),
        InstallDir(
            ".",
            BLACKLIST=["meta.ini", "NVSE/plugins/scripts/gr_Minigun-BUFFS-NERFS.txt"],
            REQUIRED_USE="vanilla-stats",
        ),
    ]
