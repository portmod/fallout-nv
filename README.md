# Fallout: New Vegas

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for Fallout: New Vegas.

* Using Portmod with Fallout: New Vegas requires you to install the prefix into the game's directory (e.g. `portmod init <prefix-name> fallout-nv <path-to-fallout-nv>`)
* The packages in this repository assume you have all the DLC 

Eventually you will be able to find details on how to get started on the Portmod [Guide](https://gitlab.com/portmod/portmod/-/wikis/home#guide).
